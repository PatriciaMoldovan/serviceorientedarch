//
//  ServerCommunication.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 26/01/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit
import Alamofire

public class ServerCommunication {
    public class func getMovies() {
        AF.request("http://localhost:5000/api/movies").responseJSON { (response) in
//            print("Request: \(String(describing: response.request))")   // original url request
//            print("Response: \(String(describing: response.response))") // http url response
//            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
//                print("JSON: \(json)") // serialized json response
                Database.shared.loadMovies(fromJson: json)
            }
        }
    }
    
    public class func findMovies(name: String, year: Int) {
        var url = "http://localhost:5000/api/findmovies"
        if (name.count > 0) {
            url = url + "?name="+name;
            if (year > 0) {
                url = url + "&year="+String(year);
            }
        } else if (year > 0) {
            url = url + "?year=" + String(year);
        }
        
        AF.request(url).responseJSON { (response) in
//                        print("Request: \(String(describing: response.request))")   // original url request
//                        print("Response: \(String(describing: response.response))") // http url response
//                        print("Result: \(response.result)")                         // response serialization result
            
            switch response.result {
            case .success(let json):
//                print("JSON: \(json)") // serialized json response
                Database.shared.loadFilterMovies(fromJson: json)
            case .failure(_):
                Database.shared.failedToLoadFilteredMovies()
            }
        }
    }
    
    public class func addMovie(name: String, year: String, imdbId: String) {
        let url = "http://localhost:5000/api/addmovie" + "?name=" + name + "&year=" + year + "&imdbId=" + imdbId
        guard let escapedUrl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
            NotificationCenter.default.post(name: .failedToAddMovie, object: nil)
            return
        }
        
        AF.request(escapedUrl).response { (response) in

            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            switch response.result {
            case .success(_):
                ServerCommunication.getMovies()
                NotificationCenter.default.post(name: .didAddMovie, object: nil)
            case .failure(_):
                NotificationCenter.default.post(name: .failedToAddMovie, object: nil)
            }
        }
    }
}
