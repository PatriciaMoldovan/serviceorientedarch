using System.Collections.Generic;
using System.Linq;
using MoviesAPI.Entities;

namespace MoviesAPI.Services
{
     public static class MoviesDbContextExtensions
     {
          public static void CreateSeedData(this MoviesDbContext context)
          {
               if (context.Movies.Any())
                    return;
               var movies = new List<Movie>()
               {
                    new Movie()
                    {
                         Name = "Avengers: Infinity War",
                         Year = "2018",
                         ImdbId = "tt4154756"

                    },
                    new Movie()
                    {
                         Name = "Thor: Ragnarock",
                         Year = "2017",
                         ImdbId = "tt3501632"
                    },
                    new Movie()
                    {
                         Name = "Black Panther",
                         Year = "2018",
                         ImdbId = "tt1825683"
                    }
               };
               context.AddRange(movies);
               context.SaveChanges();
          }
     }
}