using Microsoft.AspNetCore.Mvc;
using MoviesAPI.Entities;
using MoviesAPI.Services;

namespace MoviesAPI.Controllers
{
     [Route("api/[controller]")]
     public class AddMovieController : Controller
     {
          private MoviesDbContext _context;
          public AddMovieController(MoviesDbContext context)
          {
               _context = context;
          }
          public IActionResult GetAddMovies(string name, string year, string imdbId)
          {
              if (name == null || year == null || imdbId == null) {
                  return BadRequest("Invalid data provided");
              }
              if (name.Length == 0 || year.Length == 0 || imdbId == null) {
                  return BadRequest("Invalid data provided");
              }

              var movie = new Movie();
              movie.Name = name;
              movie.Year = year;
              movie.ImdbId = imdbId;

              _context.Add(movie);
              _context.SaveChanges();

              return Ok("success");
          }


     }
}