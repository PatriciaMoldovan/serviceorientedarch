//
//  MoviesTableViewCell.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 05/02/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imdbButton: UIButton!
    
    var movie: Movie?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setMovie(mov: Movie) {
        movie = mov
        titleLabel.text = mov.name
        subtitleLabel.text = mov.year
        imdbButton.isEnabled =  mov.imdbId != nil
    }
    
    @IBAction func imdbButtonPressed(_ sender: Any) {
        guard let url = movie?.imdbLink else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
