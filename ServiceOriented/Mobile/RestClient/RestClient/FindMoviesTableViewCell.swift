//
//  FindMoviesTableViewCell.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 05/02/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

class FindMoviesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imdbButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    var movie: Movie?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setMovie(mov: Movie) {
        movie = mov
        titleLabel.text = mov.name
        subtitleLabel.text = mov.year
        if (Database.shared.containsMovie(movie: mov)) {
            self.addButton.setTitle("Added", for: .normal)
            self.addButton.isEnabled = false
        } else {
            self.addButton.setTitle("Add", for: .normal)
            self.addButton.isEnabled = true
        }
        self.reloadInputViews()
    }
    
    @IBAction func imdbPressed(_ sender: Any) {
        guard let url = movie?.imdbLink else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func addPressed(_ sender: Any) {
        guard let movie = movie else {
            return
        }
        self.addButton.isEnabled = false
        self.addButton.setTitle("Adding", for: .normal)
        Database.shared.addMovieToDB(movie: movie)
    }
}
