using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.Services;
using MoviesAPI.Entities;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MoviesAPI.Controllers
{
     [Route("api/[controller]")]
     public class FindMoviesController : Controller
     {
         public List<Movie> Movies { get; set; }
          public FindMoviesController()
          {
              this.Movies = new List<Movie>();
          }
          public IActionResult GetFindMovies(string name, string year)
          {
            name = name != null ? name : "";
            year = year != null ? year : "";

            if (name.Length == 0 && year.Length == 0) {
                return Ok(this.Movies);
            }
            string url = "https://www.omdbapi.com/?apikey=d6aafd9a";
            if (name.Length > 0) {
                url = url + "&t=" + name;
            } 
            if (year.Length > 0) {
                url = url + "&y=" + year;
            }

            var json = "empty";
            this.Movies = new List<Movie>();
            using (WebClient webClient = new WebClient())
            {
                json = webClient.DownloadString(url);
                var values = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);

                var movie = new Movie();
                movie.Id = 0; 
                movie.Name = values["Title"].ToString();
                movie.Year = values["Year"].ToString();
                movie.ImdbId = values["imdbID"].ToString();

                this.Movies.Add(movie);
            }
            
            return Ok(this.Movies);
          }

     }
}