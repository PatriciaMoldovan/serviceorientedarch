//
//  Movie.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 26/01/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

class Movie: Any {
    let id: Int
    let name: String
    let year: String
    var imdbId: String
    
    init?(json: [String: Any]) {
        guard let name = json["name"] as? String,
            let id = json["id"] as? Int,
            let year = json["year"] as? String,
            let imdbId = json["imdbId"] as? String else {
                return nil
        }

        self.id = id
        self.name = name
        self.year = year
        self.imdbId = imdbId
    }
    
    var imdbLink: URL? {
        let strURL = "https://www.imdb.com/title/" + imdbId
        return URL(string: strURL)
    }
}
