//
//  FindMoviesTableViewController.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 26/01/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

class FindMoviesTableViewController: UITableViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    
    var movies: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Database.shared.filterMovies = []
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FindMoviesTableViewController.didUpdateData),
                                               name: .didUpdateFilter,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                              selector: #selector(FindMoviesTableViewController.didUpdateData),
                                              name: .didUpdateData,
                                              object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FindMoviesTableViewController.failedUpdateData),
                                               name: .failedUpdateFilter,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FindMoviesTableViewController.didAddMovie),
                                               name: .didAddMovie,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FindMoviesTableViewController.failedToAddMovie),
                                               name: .failedToAddMovie,
                                               object:nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didUpdateData() {
        self.movies = Database.shared.filterMovies
        self.tableView.reloadData()
    }
    
    @objc func failedUpdateData() {
        self.movies = []
        showAlertWithMessage("No movie was found for your search criteria.")
        self.tableView.reloadData()
    }
    
    @objc func didAddMovie() {
        self.tableView.reloadData()
    }
    
    @objc func failedToAddMovie() {
        showAlertWithMessage("Failed to add your movie.")
        self.tableView.reloadData()
    }
    
    @IBAction func findPressed(_ sender: Any) {
        guard let name = nameField.text else {
            showAlertWithMessage("Name must not be empty.")
            return
        }
        if (name.count == 0) {
            showAlertWithMessage("Name must not be empty.")
            return
        }
        
        guard let yearStr = yearField.text else {
            showAlertWithMessage("Year must not be empty.")
            return
        }
        var year = 0;
        if (yearStr.count != 0) {
            guard let y = Int(yearStr) else {
                showAlertWithMessage("Year must be a number.")
                return
            }
            year = y
        }
        
        Database.shared.findMovies(name: name, year: year)
    }
    
    func showAlertWithMessage(_ msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Click", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "findMoviesCell", for: indexPath) as! FindMoviesTableViewCell
        
        let movie = self.movies[indexPath.row]
        cell.setMovie(mov: movie)
        
        return cell
    }

}
