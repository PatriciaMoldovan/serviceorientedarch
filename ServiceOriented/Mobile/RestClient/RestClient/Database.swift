//
//  Database.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 26/01/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let didUpdateData = Notification.Name("didUpdateData")
    static let didUpdateFilter = Notification.Name("didUpdateFilter")
    static let failedUpdateFilter = Notification.Name("failedUpdateFilter")
    static let didAddMovie = Notification.Name("didAddMovie")
    static let failedToAddMovie = Notification.Name("failedToAddMovie")
}

class Database: Any {
    static let shared = Database()
    
    var movies: [Movie]
    var filterMovies: [Movie]
    
    private init() {
        self.movies = []
        self.filterMovies = []
    }
    
    public func loadMovies(fromJson json: Any) {
        guard let array = json as? [[String:Any]] else {
            return
        }
        
        array.forEach { (object: [String : Any]) in
            if let movie = Movie(json: object) {
                if (!movies.contains(where: { (mov) -> Bool in mov.id == movie.id})) {
                    movies.append(movie)
                }
            }
        }
        NotificationCenter.default.post(name: .didUpdateData, object: nil)
    }
    
    public func findMovies(name: String, year: Int) {
        ServerCommunication.findMovies(name: name, year: year)
    }
    
    public func loadFilterMovies(fromJson json: Any) {
        filterMovies = [];
        
        guard let array = json as? [[String:Any]] else {
            return
        }
        
        array.forEach { (object: [String : Any]) in
            if let movie = Movie(json: object) {
                if (!filterMovies.contains(where: { (mov) -> Bool in mov.imdbId == movie.imdbId})) {
                    filterMovies.append(movie)
                }
            }
        }
        NotificationCenter.default.post(name: .didUpdateFilter, object: nil)
    }
    
    public func failedToLoadFilteredMovies() {
        self.filterMovies = [];
        NotificationCenter.default.post(name: .failedUpdateFilter, object: nil)
    }
    
    public func addMovieToDB(movie: Movie) {
        ServerCommunication.addMovie(name: movie.name, year: movie.year, imdbId: movie.imdbId)
    }
    
    public func containsMovie(movie: Movie) -> Bool {
        return movies.contains(where: { (mov) -> Bool in mov.imdbId == movie.imdbId})
    }
}
