//
//  AllMoviesTableViewController.swift
//  RestClient
//
//  Created by Denisa Patricia Moldovan on 26/01/2019.
//  Copyright © 2019 Denisa Patricia Moldovan. All rights reserved.
//

import UIKit

class AllMoviesTableViewController: UITableViewController {
    
    var movies: [Movie] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.movies = Database.shared.movies
        ServerCommunication.getMovies()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AllMoviesTableViewController.didUpdateData),
                                               name: .didUpdateData,
                                               object:nil)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AllMoviesTableViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        self.tableView.addSubview(refreshControl)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didUpdateData() {
        self.movies = Database.shared.movies
        self.tableView.reloadData()
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        ServerCommunication.getMovies()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allMoviesCell", for: indexPath) as! MoviesTableViewCell

        let movie = self.movies[indexPath.row]
        cell.setMovie(mov: movie)

        return cell
    }

}
